import unittest
from coche import Coche

class TestCoche(unittest.TestCode):

    def TestColor(self):
        coche1("blanco", "audi", "a4", "4678-JBC", 10)
        self.assertEqual(coche1.ColorCoche(), "El coche es de color blanco")

    def TestMarca(self):
        coche1("blanco", "audi", "a4", "4678-JBC", 10)
        self.assertEqual(coche1.MarcaCoche(), "El coche es de la marca audi")

    def TestModelo(self):
        coche1("blanco", "audi", "a4", "4678-JBC", 10)
        self.assertEqual(coche1.ModeloCoche(), "El coche es el modelo a4")

    def TestMatricula(self):
        coche1("blanco", "audi", "a4", "4678-JBC", 10)
        self.assertEqual(coche1.MatriculaCoche(), "El coche es tiene la matrícula 4678-JBC")

if __name__ == '__main__':
    unittest.main()