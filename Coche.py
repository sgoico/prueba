class Coche:
    def __init__(self, color, marca, modelo, matri, veloc):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matri = matri
        self.veloc = veloc

    def ColorCoche(self):
        return "El coche es de color {color}".format(color = self.color)

    def MarcaCoche(self):
        return "El coche es de la marca {marca}".format(marca = self.marca)

    def ModeloCoche(self):
        return "El coche es el modelo {modelo}".format(modelo = self.modelo)

    def matricula_coche(self):
        return "La matrícula del coche es {matri}".format(matri = self.matri)

    def AcelerarCoche(self, i):

    def FrenarCoche(self, i):
        

coche1 = Coche("blanco", "audi", "a4", "4678-JBC", 10)

print(coche1.matricula_coche())